// import { Browser, Builder, By } from 'selenium-webdriver';
// // import { chrome } from 'selenium-webdriver/chrome.js';
// import assert from 'assert';
// // import pkg from 'selenium-webdriver/chrome.js';
// const { chrome } = pkg;

const { Builder, By, NoSuchElementError, Browser } = require('selenium-webdriver')
const assert = require('assert')
var chrome = require("selenium-webdriver/chrome");


describe('Home page', function() {

    this.timeout(300000);

    let driver;

    const url = 'vps-3f8dfeab.vps.ovh.net';
    console.log(url);

    beforeEach(async function() {
        console.log("before each start")
        // create driver
        let options = await new chrome.Options();
        driver = await new Builder()
          .forBrowser(Browser.CHROME)
          .setChromeOptions(options.addArguments('--headless=new'))
          .build()
        console.log("before each end")
    })

    afterEach(async function() {
        // quit driver
        await driver.quit()
    })

    it('Home page contains hello world', async () => {
      console.log('Home page contains hello world');
      // open page
      await driver.get(url)
      // verify the title contains "hello-world"
      assert((await driver.findElement(By.css('h1')).getText()).toLowerCase().includes('hello-world'));
    });

})
